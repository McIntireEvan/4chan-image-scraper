from bs4 import BeautifulSoup
from urllib import urlretrieve
import urllib2

folder = raw_input("Path:")
url = raw_input("URL:")

response = urllib2.urlopen(url)
html = response.read()

soup = BeautifulSoup(html)

download = []

for img in soup.find_all("a", {"class":"fileThumb"}):
	src = img.get("href")
	if src is not None:
		src = src.split(".org")[1].replace("s","")
		download.append("http://i.4cdn.org" + src)
for link in download:
	print link
	link_name = link.split("/")[-1]
	print link_name
	urlretrieve(link, folder + link_name)
